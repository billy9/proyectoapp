package com.corenetworks.hibernate.blog.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class Respuesta {
	@Id
	@GeneratedValue
	private long id;
	
	@ManyToOne
	@JoinColumn(name="user_id", updatable=false)
	private Profesor user;
	
	@ManyToOne
	@JoinColumn(name="post_id", updatable=false)
	private Pregunta post;
	
	@Column
	@Lob
	private String contenido;
	
	@Column
	@CreationTimestamp
	private Date fecha;

	public Respuesta(Profesor user, Pregunta post, String contenido) {
		super();
		this.user = user;
		this.post = post;
		this.contenido = contenido;
	}

	public Respuesta() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Profesor getUser() {
		return user;
	}

	public void setUser(Profesor user) {
		this.user = user;
	}

	public Pregunta getPost() {
		return post;
	}

	public void setPost(Pregunta post) {
		this.post = post;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
}
