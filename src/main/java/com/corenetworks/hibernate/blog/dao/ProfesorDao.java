package com.corenetworks.hibernate.blog.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.corenetworks.hibernate.blog.model.Profesor;

@Repository
@Transactional
public class ProfesorDao {
	
    @PersistenceContext
	private EntityManager entityManager;
    /*
     * Almacena el usuario en la base de datos
     */
    public void create(Profesor profe) {
    	entityManager.persist(profe);
    	return;
    }
    
    @SuppressWarnings("unchecked")
	public List<Profesor> getAll(){
    	return entityManager
    			.createQuery("select p from Profesor p")
    			.getResultList();
    }
    
    
    /*
     * Validar login a partir de email y password
     */
    public Profesor getByEmailAndPassword(String email, String password) {
    	Profesor resultado = null;
    	try {
    	resultado = (Profesor)  entityManager
    	    .createNativeQuery("select * FROM Profesor where  email= :email and password=md5(:password)",
    	    		Profesor.class)
    	     .setParameter("email", email)
    	     .setParameter("password", password)
    	     .getSingleResult();
    	} catch (NoResultException e) {
    		resultado = null;
		}
    	return resultado;
    	
    }
    
    
    
}
