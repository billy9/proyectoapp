package com.corenetworks.hibernate.blog.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.corenetworks.hibernate.blog.dao.ProfesorDao;



@Controller
public class UserController {
	@Autowired
	private ProfesorDao userDao;

	@GetMapping(value = "/autores")
	public String listaAutores(Model modelo) {
		modelo.addAttribute("autores", userDao.getAll());
		return "userlist";
	}
}
