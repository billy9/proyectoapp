package com.corenetworks.hibernate.blog.controller;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.blog.beans.CommentBean;
import com.corenetworks.hibernate.blog.beans.PostBean;
import com.corenetworks.hibernate.blog.beans.RegisterBean;
import com.corenetworks.hibernate.blog.dao.RespuestaDao;
import com.corenetworks.hibernate.blog.dao.PreguntaDao;
import com.corenetworks.hibernate.blog.dao.ProfesorDao;
import com.corenetworks.hibernate.blog.model.Respuesta;
import com.corenetworks.hibernate.blog.model.Pregunta;
import com.corenetworks.hibernate.blog.model.Profesor;



@Controller
public class PostController {
	@Autowired
	private PreguntaDao postDao;
	
	@Autowired
	private RespuestaDao commentDao;
	
	@Autowired
	private HttpSession httpSession;

	@GetMapping(value = "/submit")
	public String showForm(Model modelo) {
		modelo.addAttribute("post", new PostBean());
		return "submit";
	}
	
	@PostMapping(value = "/submit/newpost")
	public String submit(@ModelAttribute("post") PostBean postBean , Model model) {
		//userDao.create(new User(r.getNombre(), r.getEmail(), r.getCiudad(), r.getPassword()));
         // crear un Post
		 // Obtener el autor desde la sesión
		 // Asignar el autor al post
		 // Hacer persistente el post
		Pregunta post = new Pregunta();
		post.setTitulo(postBean.getTitulo());
		post.setContenido(postBean.getContenido());
		post.setUrl(postBean.getUrl());
		
		Profesor autor = (Profesor) httpSession.getAttribute("userLoggedIn");
		post.setAutor(autor);
		postDao.create(post);
		autor.getPosts().add(post);
		
		return "redirect:/";
	}	
	
	@GetMapping(value = "/post/{id}")
	public String detail(@PathVariable("id")  long id,   Model modelo) {
		//modelo.addAttribute("post", new PostBean());
		// Comprobar si el Post existe.
		
		Pregunta result = null;
		if ((result = postDao.getById(id)) != null) {
			modelo.addAttribute("post", result);
			modelo.addAttribute("commentForm", new CommentBean());
			return "postdetail";			
		} else
			return "redirect:/";
	}
	
	@PostMapping(value = "/submit/newComment")
	public String submitComment(@ModelAttribute("commentForm") CommentBean commentBean , Model model) {
		Profesor autor = (Profesor) httpSession.getAttribute("userLoggedIn");
		
		Respuesta comment = new Respuesta();
		comment.setUser(autor);
		
		Pregunta post = postDao.getById(commentBean.getPost_id());
		comment.setPost(post);
		comment.setContenido(commentBean.getContenido());
		commentDao.create(comment);
		post.getComments().add(comment);
		autor.getComments().add(comment);
		
		
		return "redirect:/post/"+ commentBean.getPost_id();
	}	
	
	
}
